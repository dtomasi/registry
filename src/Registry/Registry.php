<?php
/**
 * Registry - OOP-Wrapper for Arrays
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Registry;


use Dtomasi\Collections\ArrayCollection;

/**
 * Class Registry
 * @package Dtomasi\Registry
 */
class Registry
{

    /**
     * Separator for NameSpace
     * @var string
     */
    private $namespaceSeparator;

    /**
     * The Registry
     * @var ArrayCollection
     */
    protected $reg;

    /**
     * Create a Registry via static call
     * @param string $namespaceSeparator
     * @return Registry
     */
    public static function create($namespaceSeparator = '.')
    {
        return new Registry($namespaceSeparator);
    }

    /**
     * Initialize-Method
     * @param string $namespaceSeparator
     */
    public function __construct($namespaceSeparator = '.')
    {
        $this->reg = new ArrayCollection();
        $this->namespaceSeparator = $namespaceSeparator;
    }

    /**
     * @param array $entries
     */
    public function createEntriesFromArray(array $entries)
    {

        $it = new \ArrayIterator($entries);

        $it->rewind();
        while ($it->valid()) {

            $entry = new Entry($it->key());

            if (is_array($it->current())) {

                $entry->createChildrenFromArray($it->current(),$entry);
            } else {

                $entry->setValue($it->current());
            }

            $this->add($entry);

            $it->next();
        }

    }

    /**
     * Get a value at Key
     * @param $key
     * @return Entry|object|null
     */
    public function get($key)
    {
        if ($entry = static::getEntry($key)) {
            return $entry->getValue();
        }
        return null;
    }

    /**
     * Get the Entry-Object
     * @param $key
     * @return null|Entry
     */
    public function getEntry($key)
    {
        $entry = $this->getEntryAtNamespace($key);

        if ($entry instanceof Entry) {
            return $entry;
        }

        return null;
    }

    /**
     * Get a Entry at given Namespace string
     * @param string $strNamespace
     * @return Entry
     */
    protected function getEntryAtNamespace($strNamespace)
    {
        $arrNamespace = $this->splitNamespace($strNamespace);

        $arrEntries = array();

        if (!$this->reg->has($arrNamespace[0])) {
            return false;
        }

        /**
         * @var $arrEntries Entry[]
         */
        $arrEntries[] = $this->reg->get($arrNamespace[0]);

        $current = $arrEntries[0];

        while ($currentKey = next($arrNamespace)) {

            if ($current->hasChild($currentKey)) {
                $arrEntries[] = $current = $current->getChild($currentKey);
                continue;
            }

        }

        return end($arrEntries);
    }

    /**
     * Split the String
     * @param $strNamespace
     * @return array
     */
    protected function splitNamespace($strNamespace)
    {
        if (strpos($strNamespace, $this->namespaceSeparator) !== false) {
            return explode($this->namespaceSeparator, $strNamespace);
        }
        return array($strNamespace);
    }

    /**
     * @param Entry $entry
     * @param null|string $strNamespace
     * @throws \InvalidArgumentException
     */
    public function add(Entry $entry, $strNamespace = null)
    {

        if ($strNamespace !== null && strpos($strNamespace, $this->namespaceSeparator) !== false) {
            $found = $this->getEntryAtNamespace($strNamespace);

            if ($found instanceof Entry) {
                $found->addChild($entry);
            } else {
                throw new \InvalidArgumentException('namespace does not exist');
            }
        }

        $this->reg->set($entry->getKey(), $entry);
    }

    /**
     * Get the Registry-Tree
     * @return ArrayCollection
     */
    public function getTree()
    {
        return $this->reg;
    }

    /**
     * Get the Tree as Array
     *
     * @return array
     */
    public function toArray()
    {

        return $this->buildArray($this->reg);
    }

    /**
     * Build the Export Array
     *
     * @param ArrayCollection $collection
     * @param array $array
     * @return array
     */
    protected function buildArray(ArrayCollection $collection, $array = array())
    {

        $collection->rewind();
        while ($collection->valid()) {

            /** @var Entry $entry */
            $entry = $collection->current();

            if ($entry->hasChildren()) {
                $array[$entry->getKey()] = $this->buildArray($entry->getChildren(), $array);
            } else {
                $array[$entry->getKey()] = $entry->getValue();
            }

            $collection->next();
        }

        return $array;
    }
} 