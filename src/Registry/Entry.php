<?php
/**
 * Registry - OOP-Wrapper for Arrays
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Registry;

use Dtomasi\Collections\ArrayCollection;

/**
 * Class Entry
 * @package Dtomasi\Registry
 */
class Entry
{
    /**
     * The Entry´s key
     * @var string
     */
    protected $key;

    /**
     * The Entry´s value
     * @var mixed|null
     */
    protected $value;

    /**
     * The Collection of Children
     * @var ArrayCollection
     */
    protected $children;

    /**
     * The Parent-Entry-Object
     * @var Entry
     */
    protected $parent;

    /**
     * Create A Entry via static call
     * @param $key
     * @param null $value
     * @param array $arrChildren
     * @return Entry
     */
    public static function create($key, $value = null, array $arrChildren = array())
    {
        return new Entry($key, $value, $arrChildren);
    }

    /**
     * Create a Entry
     * @param $key
     * @param null $value
     * @param Entry[] $arrChildren
     */
    public function __construct($key, $value = null, array $arrChildren = array())
    {
        $this->key = $key;
        $this->value = $value;
        $this->children = new ArrayCollection();

        foreach ($arrChildren as $child) {
            $this->addChild($child);
        }
    }

    /**
     * Set the value
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get Entry´s key
     * @return array|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Get Entry´s value
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add a Child-Entry
     * @param Entry $entry
     */
    public function addChild(Entry $entry)
    {
        $entry->setParent($this);
        $this->children->set($entry->getKey(), $entry);
    }

    /**
     * Check has Child
     * @param $key
     * @return bool
     */
    public function hasChild($key)
    {
        return $this->children->has($key);
    }

    /**
     * Check has Children
     * @return bool
     */
    public function hasChildren()
    {
        return !$this->children->isEmpty();
    }

    /**
     * Get a Child
     * @param $key
     * @return Entry|null
     */
    public function getChild($key)
    {
        return $this->children->get($key);
    }

    /**
     * Get all Children
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get all Children as array
     * @return Entry[]
     */
    public function getChildrenArray()
    {
        return $this->children->toArray();
    }

    /**
     * Remove a Child
     * @param $key
     */
    public function removeChild($key)
    {
        $this->children->remove($key);
    }

    /**
     * Set Parent-Entry
     * @param Entry $entry
     */
    public function setParent(Entry $entry)
    {
        $this->parent = $entry;
    }

    /**
     * Get Parent-Entry
     * @return Entry
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param array $entries
     * @param null|Entry $baseEntry
     */
    public function createChildrenFromArray(array $entries, $baseEntry = null) {

        $it = new \ArrayIterator($entries);

        $it->rewind();
        while ($it->valid()) {

            $entry = new Entry($it->key());

            if (is_array($it->current())) {

                $this->createChildrenFromArray($it->current(), $entry);
            } else {

                $entry->setValue($it->current());
            }

            if ($baseEntry == null) {

                $this->addChild($entry);
            } else {

                $baseEntry->addChild($entry);
            }

            $it->next();
        }
    }
}
