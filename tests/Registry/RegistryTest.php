<?php
/**
 *
 * User: Dominik Tomasi
 * Date: 06.07.14
 * Time: 09:30
 */

namespace Dtomasi\Tests\Registry;

use Dtomasi\Registry\Entry;
use Dtomasi\Registry\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Registry
     */
    public $reg;

    public function setUp()
    {
        $this->reg = new Registry();
    }

    public function testAddAndGet()
    {

        $this->reg->add(new Entry('foo', 'bar'));

        $this->assertEquals('bar', $this->reg->get('foo'));

    }

    public function testGetValue()
    {

        $root =
            new Entry('api', 'foo', array(
                new Entry('router', 'bar', array(
                    new Entry('routes', array('foo', 'bar', 'baz'))
                ))
            ));

        $this->reg->add($root);

        $this->assertEquals(array('foo', 'bar', 'baz'), $this->reg->get('api.router.routes'));

    }

    public function testGetEntry()
    {

        $root =
            new Entry('api', 'foo', array(
                new Entry('router', 'bar', array(
                    new Entry('routes', array('foo', 'bar', 'baz'))
                ))
            ));

        $this->reg->add($root);

        $this->assertInstanceOf('Dtomasi\Registry\Entry', $this->reg->getEntry('api.router.routes'));
    }

    public function testChangeNamespaceSeparator()
    {

        $this->reg = new Registry('\\');

        $root =
            new Entry('api', 'foo', array(
                new Entry('router', 'bar', array(
                    new Entry('routes', array('foo', 'bar', 'baz'))
                ))
            ));

        $this->reg->add($root);

        $this->assertInstanceOf('Dtomasi\Registry\Entry', $this->reg->getEntry('api\router\routes'));

    }

    public function testFillRegistry()
    {

        for ($i = 0; $i < 100; $i++) {
            $this->reg->add(

                new Entry('foo-' . $i, 'bar-' . $i, array(
                    new Entry('bar-' . $i, 'baz-' . $i * 10, array(
                        new Entry('baz-' . $i, 'foo-' . $i * 100)
                    ))
                )));
        }

        $this->assertEquals('baz-100', $this->reg->get('foo-10.bar-10'));
    }

    public function testAddToChild()
    {

        for ($i = 0; $i < 100; $i++) {
            $this->reg->add(

                new Entry('foo-' . $i, 'bar-' . $i, array(
                    new Entry('bar-' . $i, 'baz-' . $i * 10, array(
                        new Entry('baz-' . $i, 'foo-' . $i * 100)
                    ))
                )));
        }

        $this->reg->getEntry('foo-10.bar-10')->addChild(new Entry('foo', 'bar'));

        $this->assertTrue($this->reg->getEntry('foo-10.bar-10')->hasChild('foo'));
        $this->assertEquals('bar', $this->reg->get('foo-10.bar-10.foo'));
    }

    public function testLoadArray()
    {

        $arrParams = array(
            'foo' => 'bar',
            'bar' => array(
                'foo' => 'bar',
                'bar' => array(
                    'foo' => 'bar',
                    'bar' => new \stdClass()
                )
            ),
            'baz' => new \stdClass()

        );
        $this->reg->createEntriesFromArray($arrParams);

        return $arrParams;
    }

    public function testToArray()
    {
        $importArray = $this->testLoadArray();
        $exportArray = $this->reg->toArray();

        $this->assertEquals($importArray,$exportArray);

    }
}
 