<?php
/**
 *
 * User: Dominik Tomasi
 * Date: 04.07.14
 * Time: 15:08
 */

namespace Dtomasi\Tests\Registry;

use Dtomasi\Registry\Entry;

require_once 'vendor/autoload.php';


class EntryTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateEntry()
    {
        $entry = new Entry('foo', array('bar' => 'baz'));

        $this->assertEquals('foo', $entry->getKey());
        $this->assertTrue(is_array($entry->getValue()));

        $val = $entry->getValue();
        $this->assertEquals('baz', $val['bar']);
    }

    public function testHasChildren()
    {
        $entry = new Entry('foo');
        $this->assertFalse($entry->hasChildren());
    }

    public function testSetChild()
    {
        $base = new Entry('parent');
        $child = new Entry('child');

        $base->addChild($child);
        $this->assertTrue($base->hasChildren());
        $this->assertTrue($base->hasChild('child'));

        $this->assertEquals('child', $base->getChild('child')->getKey());
    }

    public function testGetChildren()
    {
        $base = new Entry('parent');
        for ($i = 0; $i < 100; $i++) {
            $child = new Entry('child');
            $base->addChild($child);
        }

        $this->assertInstanceOf('\Dtomasi\Collections\ArrayCollection', $base->getChildren());
        $this->assertTrue(is_array($base->getChildrenArray()));
    }

    public function testSetAndGetParent()
    {
        $base = new Entry('parent');
        $child = new Entry('child');

        $base->addChild($child);

        $this->assertEquals('parent', $base->getChild('child')->getParent()->getKey());
    }

    public function testCreateChildrenFromArray()
    {

        $testArray = array(

            'firstLevel-1' => array(
                'secondLevel-1' => 'someValue',
                'secondLevel-2' => array(
                    'thirdLevel-1' => 'someValue'
                )
            ),
            'firstLevel-2' => 'someValue'

        );

        $base = new Entry('parent');
        $base->createChildrenFromArray($testArray);

    }

}
 